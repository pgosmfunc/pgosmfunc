--  Format string with French IGN toponymy rules
--  Copyright (C) 2010,2011  Rodolphe Quiédeville <rodolphe@quiedeville.org>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.

 
DROP FUNCTION IF EXISTS osm_capitalize(text);

CREATE FUNCTION osm_capitalize (text) RETURNS text AS $$
# Lowercase and split the input string
@alphas = split(/\s+/, lc($_[0]));
@alpha = ();

foreach $tata (@alphas) {

    while (index ($tata,'-') > 0) {
	$index = index ($tata,'-');
	$begin = substr ($tata, 0, $index);
	print "Begin : $begin\n";
	push @alpha, $begin;
	push @alpha, '-';
	$tata = substr ($tata, 1+ $index - length($tata));
    } 
    push @alpha, $tata;
}
    
# List of words that will not be uppercase
@topos = ('le','la','les','du','des','de','à','au','aux','lès','lez','sur','en','ès');

$str = '';
$i = 0;
$last = ' ';
foreach $al (@alpha) {
    $part = '';
    $sep = ' ';
    $sep = '' if ($last eq '-');
	
    if (length($al) > 0) {
	if ($al eq '-') {
	    $str = $str.'-';
	    $last = '-';
	} else {
	    $last = '';
	    if ($i > 0) {
		foreach $top (@topos) {
		    if ($al eq $top) {
			$part = $al; 	   	    
		    }	
		}
		
		if ($part eq '') {
		    if (substr($al,1,1) eq '\'') {
			$str = $str . $sep. substr($al,0,2) . ucfirst(substr($al, 2-length($al) ));
		    } else {
			$str = $str . $sep. ucfirst($al);
		    }
		} else {	
		    $str = $str.$sep.$part; 
		}
	    } else {
		# The first part is always uppercased
		if (substr($al,1,1) eq '\'') {
		    $str = ucfirst( substr($al,0,2)) . ucfirst(substr($al, 2-length($al) ));
		} else {
		    $str = ucfirst($al);
		}
	    }
	}
	$i++;
    }
}
return $str;
$$ LANGUAGE plperl;
